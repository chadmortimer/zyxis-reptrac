﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization.Formatters.Binary;

namespace RepTracWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult MapTest()
        {
            return View();
        }

        public JsonResult DeviceList()
        {

            using (var db = new Models.reptracEntities())
            {

                var devices = db.Devices.Where(d=> !d.description.Contains("simulator")).OrderBy(d=>d.description).Select( device => new { DeviceName = device.description,  DeviceID = device.id}).ToList();

                return Json(devices, JsonRequestBehavior.AllowGet);

            }

            
        }


        public JsonResult PosList(string date, string deviceid)
        {

            using (var db = new Models.reptracEntities())
            {
                int did = int.Parse(deviceid);

                DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                DateTime dt2 = dt.AddDays(1);

                var pos = db.vw_location_summary.Where(l =>l.device_id == did && l.gps_ts >= dt && l.gps_ts < dt2).OrderBy(d => d.gps_ts)
                    .Select(location => new {
                        name = location.gps_ts,
                        latlng = new List<decimal>() { (decimal) location.gps_lat, (decimal) location.gps_long} ,
                        speed = location.gps_speed,
                        acc = location.gps_accuracy,
                        alt = location.gps_alt,
                        heading = location.gps_heading,
                        location = location.gps_location,
                        devicename = location.description,
                        devicetype = location.device_type,
                        time = location.gps_ts,
                        rectime = location.received_ts,
                        hash = location.hash
                    }).ToList();

                var poslist = pos.Select(p => new {
                        name = ((DateTime)p.name).ToString("dd/MM/yyyy HH:mm:ss"), 
                        latlng = p.latlng,
                        speed = (p.speed >=0 ) ? ((double) p.speed) * 3.6 : -1,
                        speedstr = (p.speed >= 0) ? (((double)p.speed) * 3.6).ToString("N2") : "na",
                        acc = p.acc ,
                        alt = p.alt ,
                        heading = p.heading ,
                        location = p.location,
                        devicename = p.devicename ,
                        devicetype = p.devicetype,
                        time = p.time,
                        rectime = p.rectime ,
                        hash = p.hash ,
                        }
                    );

                var jsonres = Json(poslist);

                return Json(poslist, JsonRequestBehavior.AllowGet);

            }


        }
    }
}
